import {
  ensureFile,
  globToRegExp,
  join,
  readLines,
  relative,
  walk,
} from "./deps.ts";

const time_re =
  /^\s*(\d{2}:\d{2}:\d{2},\d{3})\s{1,}-->\s{1,}(\d{2}:\d{2}:\d{2},\d{3})\s*$/;
const tag_re = /<[^>]+>/g;

function stripTags(line: string) {
  return line.replace(tag_re, "");
}

function isSub(line: string, i: string) {
  return line !== "" && line !== i;
}

async function main() {
  performance.mark("start");
  const entryIter = walk(".", {
    includeDirs: false,
    exts: ["srt"],
    skip: [globToRegExp("clean/*")],
  });

  let fileCount = 0;
  for await (const entry of entryIter) {
    let i = 1;
    const lines: string[] = [];
    const reader = await Deno.open(entry.path);
    const lineIter = readLines(reader, { encoding: "windows-1252" });

    for await (const line of lineIter) {
      const i_str = i.toString();
      const timeMatch = line.match(time_re);
      if (timeMatch) {
        const time = `${timeMatch[1]} --> ${timeMatch[2]}`;
        lines.push("", i_str, time);
        i++;
      } else if (isSub(line, i_str)) {
        lines.push(stripTags(line));
      }
    }
    Deno.close(reader.rid);

    const relPath = relative(".", entry.path);
    const outPath = join("clean", relPath);
    const text = lines.join("\n").trim() + "\n";
    await ensureFile(outPath);
    Deno.writeTextFile(outPath, text);
    fileCount++;
  }
  const { duration } = performance.measure("end", "start");
  console.log("Processed %d files in %dms", fileCount, duration);
}

if (import.meta.main) {
  main();
}
